package de.bnware.kripfganz;

import de.bnware.kripfganz.norm.NormCalculator;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author Nicky Gruner
 */
public class ErrorCalculator {

    private final NormCalculator calculator;

    public ErrorCalculator (NormCalculator calculator) {
        this.calculator = calculator;
    }

    public double calculateRelativeError (RealMatrix A, RealMatrix delA) {
        double normA = calculator.calculateNorm(A);
        System.out.print("||A|| = ");
        System.out.println(normA);
        double normDelA = calculator.calculateNorm(delA);
        System.out.print("||delA|| = ");
        System.out.println(normDelA);
        double conditionNumberA = calculateConditionNumber(A, normA);
        System.out.print("K(A) = ");
        System.out.println(conditionNumberA);
        double relativeInputError = normDelA / normA;
        System.out.print("||delA||/||A|| = ");
        System.out.println(relativeInputError);
        return (conditionNumberA / (1-conditionNumberA * relativeInputError)) * relativeInputError;
    }

    private double calculateConditionNumber (RealMatrix matrix, double norm) {
        return norm * calculator.calculateNorm(MatrixUtils.inverse(matrix));
    }
    
}
