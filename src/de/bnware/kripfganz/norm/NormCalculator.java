package de.bnware.kripfganz.norm;

import org.apache.commons.math3.linear.RealMatrix;

/**
 * interface to implement different NormCalculator's
 *
 * @author Nicky Gruner
 */
public interface NormCalculator {

    /**
     * calculates the norm of the given matrix (||matrix||)
     *
     * @param matrix the matrix to calculate the norm for
     * @return the calculated norm
     */
    double calculateNorm (RealMatrix matrix);
    
}
