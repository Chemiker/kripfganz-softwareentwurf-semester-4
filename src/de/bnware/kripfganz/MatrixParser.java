package de.bnware.kripfganz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.genetics.InvalidRepresentationException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author Nicky Gruner
 */
public class MatrixParser {

    private static final Pattern SPLIT = Pattern.compile("\t");

    /**
     * parses the string into a RealMatrix object.
     * rows must be separated by line break, columns by tab
     *
     * @param input    the string to parse
     * @return the parsed RealMatrix object
     * @throws NumberFormatException if some values could not be parsed to double
     * @throws NoDataException if there are no rows or no columns
     */
    public static RealMatrix read (String input) {
        try (StringReader reader = new StringReader(input)) {
            return read(reader);
        } catch (IOException e) {
            throw new InvalidRepresentationException(LocalizedFormats.CANNOT_PARSE, input, 0);
        }
    }

    /**
     * reads in the given file and parses the content into a RealMatrix object.
     * rows must be separated by line break, columns by tab
     *
     * @param file    the file to read in
     * @return the parsed RealMatrix object
     * @throws IOException if reading fails
     * @throws NumberFormatException if some values could not be parsed to double
     * @throws NoDataException if there are no rows or no columns
     */
    public static RealMatrix read (File file) throws IOException {
        try (FileReader reader = new FileReader(file)) {
            return read(reader);
        }
    }

    /**
     * reads from the given reader and parses the content into a RealMatrix object.
     * rows must be separated by line break, columns by tab
     *
     * @param input    the reader to read from
     * @return the parsed RealMatrix object
     * @throws IOException if reading fails
     * @throws NumberFormatException if some values could not be parsed to double
     * @throws NoDataException if there are no rows or no columns
     */
    public static RealMatrix read (Reader input) throws IOException {
        List<List<Double>> matrix = new LinkedList<>();
        // use object instead of int because lamdas don't allow setting outer variables
        AtomicInteger cols = new AtomicInteger(0);
        try (BufferedReader reader = new BufferedReader(input)) {
            reader.lines().forEach(line -> {
                List<Double> row = new LinkedList<>();
                SPLIT.splitAsStream(line).forEach(value -> row.add(new Double(value)));
                cols.set(Math.max(cols.get(), row.size()));
                matrix.add(row);
            });
        }
        double[][] buffer = new double[matrix.size()][cols.get()];
        int i = 0;
        for (List<Double> row : matrix) {
            int j = 0;
            for (double value : row) {
                buffer[i][j++] = value;
            }
            i++;
        }
        return new Array2DRowRealMatrix(buffer, false);
    }
}
