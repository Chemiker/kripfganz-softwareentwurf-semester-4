package de.bnware.kripfganz.norm;

import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author Nicky Gruner
 */
public class MaximumNorm implements NormCalculator {
    
    @Override
    public double calculateNorm (RealMatrix matrix) {
        double norm = 0;
        for (int i = 0; i < matrix.getRowDimension(); i++) {
            double[] row = matrix.getRow(i);
            double rowSum = 0;
            for (double value : row) {
                rowSum += Math.abs(value);
            }
            norm = Math.max(norm, rowSum);
        }
        return norm;
    }
}
