package de.bnware.kripfganz;

import java.io.File;
import java.io.IOException;

import de.bnware.kripfganz.norm.MaximumNorm;
import de.bnware.kripfganz.norm.SpektralNorm;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author Nicky Gruner
 */
public class Launcher {

    public static void main (String[] args) throws IOException {
        // check if argument count is correct
        if (args.length != 2) {
            printUsage();
        }
        // read in matrix A
        RealMatrix A = MatrixParser.read(new File(args[0]));
        // read in matrix delA
        RealMatrix delA = MatrixParser.read(new File(args[1]));

        System.out.println("Using Maximumnorm...");
        ErrorCalculator calculator = new ErrorCalculator(new MaximumNorm());
        double relativeErrorMaximumNorm = calculator.calculateRelativeError(A, delA);
        System.out.println();
        System.out.print("||delx||/||x|| = ");
        System.out.println(relativeErrorMaximumNorm);

        System.out.println();
        System.out.println("Using Spektralnorm (use VectorIteration to calculate lambda)...");
        calculator = new ErrorCalculator(new SpektralNorm());
        double relativeErrorSpektralNorm = calculator.calculateRelativeError(A, delA);
        System.out.println();
        System.out.print("||delx||/||x|| = ");
        System.out.println(relativeErrorSpektralNorm);

        System.out.println();
        System.out.println("Using Spektralnorm (use library to calculate lambda)...");
        calculator = new ErrorCalculator(new SpektralNorm(true));
        double relativeErrorSpektralNormInternal = calculator.calculateRelativeError(A, delA);
        System.out.println();
        System.out.print("||delx||/||x|| = ");
        System.out.println(relativeErrorSpektralNormInternal);

        System.out.println();
        System.out.print("Difference between SpektralNorm internal and Vektoriteration: ");
        System.out.println(Math.abs(relativeErrorSpektralNormInternal - relativeErrorSpektralNorm));
        System.out.print("Difference between SpektralNorm and MaximumNorm: ");
        System.out.println(Math.abs(relativeErrorMaximumNorm - relativeErrorSpektralNorm));
    }

    private static void printUsage () {
        System.out.println("Usage: java -jar kripfganz.jar <file_containing_matrix_A> <file_containing_matrix_delA>");
        System.exit(1);
    }

}
