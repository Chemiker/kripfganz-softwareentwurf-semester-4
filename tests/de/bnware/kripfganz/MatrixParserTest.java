package de.bnware.kripfganz;

import java.util.Locale;

import org.apache.commons.math3.exception.NoDataException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertThat;

/**
 * @author Nicky Gruner
 */
public class MatrixParserTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testNoRows () throws Exception {
        expectedException.expect(NoDataException.class);
        expectedException.expectMessage(LocalizedFormats.AT_LEAST_ONE_ROW.getLocalizedString(Locale.US));
        String input = "";
        RealMatrix matrix = MatrixParser.read(input);
    }

    @Test
    public void testNoColumns () throws Exception {
        expectedException.expect(NoDataException.class);
        expectedException.expectMessage(LocalizedFormats.AT_LEAST_ONE_COLUMN.getLocalizedString(Locale.US));
        String input = "\n\n";
        RealMatrix matrix = MatrixParser.read(input);
    }

    @Test
    public void testInvalidNumbers () throws Exception {
        expectedException.expect(NumberFormatException.class);
        String input = "1\t2\t3\n4\ttest";
        RealMatrix matrix = MatrixParser.read(input);
    }

    @Test
    public void testParse () throws Exception {
        double[][] data = generateArrayMatrix(3, 3);
        String input = generateStringFromMatrix(data);
        RealMatrix matrix = MatrixParser.read(input);
        double[][] actual = ((Array2DRowRealMatrix) matrix).getDataRef();
        assertNotSame("matrix not copied", data, actual);
        assertThat("unexpected matrix values", actual, equalTo(data));
    }

    private double[][] generateArrayMatrix (int rows, int cols) {
        double[][] matrix = new double[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = cols * i + j + 1;
            }
        }
        return matrix;
    }

    private String generateStringFromMatrix (double[][] matrix) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                buffer.append(matrix[i][j]);
                buffer.append('\t');
            }
            buffer.setCharAt(buffer.length() - 1, '\n');
        }
        buffer.deleteCharAt(buffer.length() - 1);
        return buffer.toString();
    }
}