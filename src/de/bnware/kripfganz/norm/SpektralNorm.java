package de.bnware.kripfganz.norm;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

/**
 * @author Nicky Gruner
 */
public class SpektralNorm implements NormCalculator {

    private boolean useInternal;

    public SpektralNorm () {
        this(false);
    }

    public SpektralNorm (boolean useInternal) {
        this.useInternal = useInternal;
    }

    @Override
    public double calculateNorm (RealMatrix matrix) {
        final RealMatrix aTa = matrix.preMultiply(matrix.transpose());
        double lambdamax;
        if (useInternal) {
            lambdamax = calculateInternal(aTa);
        } else {
            lambdamax = calculateVectorIteration(aTa, matrix.getRowDimension());
        }
        return Math.sqrt(lambdamax);
    }

    private double calculateVectorIteration (RealMatrix aTa, int n) {
        final double eps = 10E-14;
        double lambda = 0, lambdamax = 0;
        //initialise x with (1, 0, ..., 0)
        RealMatrix x = new Array2DRowRealMatrix(n, 1);
        x.setEntry(0, 0, 1);
        // iterate (maxIterations: 30)
        for (int k = 0; k < 30; k++) {
            //y = Ax
            RealMatrix y = aTa.multiply(x);
            // lambda = xy
            lambda = x.transpose().multiply(y).getEntry(0, 0);
            if (Math.abs(lambda - lambdamax) < eps) {
                lambdamax = lambda;
                break;
            }
            lambdamax = lambda;
            x = y.scalarMultiply(Math.signum(lambda) / calculateEuklidNorm(y.getColumn(0)));
        }
        System.out.print("LambdaMax: ");
        System.out.println(lambdamax);
        return lambdamax;
    }

    private double calculateEuklidNorm (double[] v) {
        double norm = 0;
        for (double value : v) {
            norm += value * value;
        }
        return Math.sqrt(norm);
    }

    private double calculateInternal (RealMatrix aTa) {
        double lambdamax = 0;
        EigenDecomposition decomposition = new EigenDecomposition(aTa);
        double[] eigenvalues = decomposition.getRealEigenvalues();
        for (double eigenvalue : eigenvalues) {
            lambdamax = Math.max(lambdamax, eigenvalue);
        }
        System.out.print("LambdaMax: ");
        System.out.println(lambdamax);
        return lambdamax;
    }
}
